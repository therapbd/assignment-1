# README #

This is the first assignment of training campaign at Therap Services (BD) Ltd. A Software export company in Bangladesh.

### Log Parser ###

Parses a .log file and extracts the following information:

* Total Get and Post posts.
* Total number of unique URIs.
* Total response time.

### Assumption ###

* The log file contains only 24 hours of data.
* Data is in 24 hour format.
* Respons time is given in milliseconds.
