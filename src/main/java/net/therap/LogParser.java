package net.therap;

import net.therap.controller.LogFileParser;
import net.therap.model.LogSummary;
import net.therap.view.LogView;

import java.util.List;
import java.util.Map.Entry;

/**
 * @author aditya.chakma
 * @since 9/3/21
 */
public class LogParser {

    private final String SORT_FLAG = "--sort";
    private boolean sort = false;

    public static void main(String[] args) {
        String fileName;
        LogParser lp = new LogParser();
        lp.verifyArgs(args);
        fileName = args[0];
        List<Entry<Integer, LogSummary>> data = lp.parseLog(fileName);
        LogView lv = new LogView();
        lv.logView(data);
    }

    public void verifyArgs(String[] args) {
        if (args.length == 0) {
            System.out.println("Please pass a filename as first argument.");
            System.exit(-1);
        } else if (args.length >= 2) {
            sort = args[1].equals(SORT_FLAG);
        }
    }

    public List<Entry<Integer, LogSummary>> parseLog(String fileName) {
        LogFileParser lfp = new LogFileParser(fileName, sort);
        return lfp.parse();
    }
}
