package net.therap.view;

import net.therap.model.LogSummary;
import net.therap.model.Time;
import net.therap.model.TimeConverter;

import java.util.List;
import java.util.Map.Entry;

/**
 * @author aditya.chakma
 * @since 10/3/21
 */
public class LogView {

    public void logView(List<Entry<Integer, LogSummary>> data) {
        for (Entry<Integer, LogSummary> singleEntry : data) {
            final String anti = "am.";
            final String post = "pm.";
            String firstHour = post;
            String secondHour = post;
            LogSummary record = singleEntry.getValue();
            int hour = record.getTime();
            TimeConverter clock = new TimeConverter();
            Time time12 = clock.to12(hour);

            if (time12.getMeridian().equals(clock.TIME_AM)) {
                firstHour = anti;
            }

            if (hour == 23 || hour < 11) {
                secondHour = anti;
            }

            System.out.print(time12.getHour() + firstHour + "-" + (clock.to12(hour + 1).getHour()) + secondHour + " ");
            System.out.print(record.getTotalGet() + "/" + record.getTotalPost() + " ");
            System.out.println(record.getTotalUniqueUri() + " " + record.getTotalResponseTime() + "ms");
        }
    }
}
