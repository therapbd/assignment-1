package net.therap.controller;

import net.therap.model.ColoredText;
import net.therap.model.LogSummary;
import net.therap.model.TimeComparator;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author aditya.chakma
 * @since 9/3/21
 */
enum RequestType {GET, POST, NONE}

public class LogFileParser {

    public static final int HOURS_IN_DAY = 24;
    private File file;
    private Map<Integer, LogSummary> data;
    private boolean sort;

    public LogFileParser(String filename, boolean sort) {
        this.sort = sort;
        this.file = new File(filename);
        this.data = new HashMap<>();

        if (!file.exists()) {
            System.out.println("File does not exist. Please enter a valid file name");
            System.exit(-1);
        }
    }

    public List<Entry<Integer, LogSummary>> parse() {
        readFile();
        List<Entry<Integer, LogSummary>> dataList = new ArrayList<>(data.entrySet());

        if (this.sort) {
            dataList.sort(new TimeComparator());
        }

        return dataList;
    }

    public void readFile() {
        FileReader fr;
        BufferedReader br;
        String record;
        List<Set<String>> uriSets = new ArrayList<>(HOURS_IN_DAY);

        for (int i = 0; i < this.HOURS_IN_DAY; i++) {
            uriSets.add(new HashSet<String>());
        }

        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);

            while ((record = br.readLine()) != null) {
                String uri = parseUri(record);
                RequestType postType = getGP(record);
                int totalRT = getRT(record);
                int[] time = getTime(record);
                int hour = time[0];

                if (!data.containsKey(new Integer(hour))) {
                    data.put(new Integer(hour), new LogSummary());
                }

                LogSummary tempRecord = data.get(new Integer(hour));
                tempRecord.setTime(hour);

                switch (postType) {
                    case GET:
                        tempRecord.setTotalGet(tempRecord.getTotalGet() + 1);
                        break;
                    case POST:
                        tempRecord.setTotalPost(tempRecord.getTotalPost() + 1);
                        break;
                    case NONE:
                        //do nothing
                }

                if (uri != null) {
                    uriSets.get(hour).add(uri);
                }

                tempRecord.setTotalResponseTime(tempRecord.getTotalResponseTime() + totalRT);
            }

            for (int i = 0; i < HOURS_IN_DAY; i++) {
                if (data.containsKey(i)) {
                    data.get(i).setTotalUniqueUri(uriSets.get(i).size());
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
            System.exit(-1);
        } catch (IOException e) {
            System.out.println(e);
            System.exit(-1);
        }
    }

    private String parseUri(String s) {
        final String URI_PARSE_STRING = "URI[^,]*";
        Pattern p = Pattern.compile(URI_PARSE_STRING);
        Matcher m = p.matcher(s);

        if (m.find()) {
            return s.substring(m.start(), m.end());
        }

        return null;
    }

    private RequestType getGP(String s) {
        final String POST_PARSE_STRING = "P,";
        final String GET_PARSE_STRING = "G,";
        Pattern p = Pattern.compile(POST_PARSE_STRING);
        Matcher m = p.matcher(s);

        if (m.find()) {
            return RequestType.POST;
        }

        // Find Get
        p = Pattern.compile(GET_PARSE_STRING);
        m = p.matcher(s);

        if (m.find()) {
            return RequestType.GET;
        }

        return RequestType.NONE;
    }

    private int getRT(String s) {
        final String RT_PARSE_STRING = "time=[0-9]+ms";
        Pattern p = Pattern.compile(RT_PARSE_STRING);
        Matcher m = p.matcher(s);

        if (m.find()) {
            String subS = s.substring(m.start(), m.end());
            p = Pattern.compile("[0-9]+");
            m = p.matcher(subS);
            if (!m.find()) {
                System.out.println(ColoredText.RED + "Parsing Error! Should have contained a number" + ColoredText.RESET);
                System.exit(-1);
            }

            return Integer.parseInt(subS.substring(m.start(), m.end()));
        }

        return 0;
    }

    private int[] getTime(String s) {
        final String TIME_PARSE_STRING = "([0-9]{2}:){2}[0-9]{2},[0-9]{3}";
        final Pattern p = Pattern.compile(TIME_PARSE_STRING);
        final Matcher m = p.matcher(s);
        int[] time = new int[4];
        String parsedString = "";

        if (m.find()) {
            parsedString = s.substring(m.start(), m.end());
            String[] splited = parsedString.split(":");
            time[0] = Integer.parseInt(splited[0]);
            time[1] = Integer.parseInt(splited[1]);
            time[2] = Integer.parseInt(splited[2].substring(0, 2));      // 01,345
            time[3] = Integer.parseInt(splited[2].substring(3));
        } else {
            System.out.println(ColoredText.RED + "Time parsing error! Check the format xx:xx:xx,xxx" + ColoredText.RESET);
            System.exit(-1);
        }

        return time;
    }
}
