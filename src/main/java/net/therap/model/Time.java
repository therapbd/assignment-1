package net.therap.model;

/**
 * @author aditya.chakma
 * @since 10/3/21
 */
public class Time {

    private int hour;
    private String meridian;

    public int getHour() {
        return this.hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getMeridian() {
        return this.meridian;
    }

    public void setMeridian(String meridian) {
        this.meridian = meridian;
    }
}
