package net.therap.model;

/**
 * @author aditya.chakma
 * @since 3/15/21
 */
public class LogSummary {

    private int time;
    private int totalGet;
    private int totalPost;
    private int totalUniqueUri;
    private int totalResponseTime;

    public void setTime(int time) {
        if (time < 0 || time > 23) {
            System.out.println(ColoredText.RED + "Time format error! Time must be in range of 0-23" + ColoredText.RESET);
            System.exit(-1);
        }
        this.time = time;
    }

    public int getTime() {
        return time;
    }

    public void setTotalGet(int totalGet) {
        if (totalGet >= 0) {
            this.totalGet = totalGet;
        } else {
            System.out.println(ColoredText.YELLOW + "Total get negative. Ignoring entry!" + ColoredText.RESET);
        }
    }

    public int getTotalGet() {
        return totalGet;
    }

    public void setTotalPost(int totalPost) {
        if (totalPost >= 0) {
            this.totalPost = totalPost;
        } else {
            System.out.println(ColoredText.YELLOW + "Total post negative. Ignoring Entry!" + ColoredText.RESET);
        }
    }

    public int getTotalPost() {
        return totalPost;
    }

    public void setTotalUniqueUri(int totalUniqueUri) {
        if (totalPost >= 0) {
            this.totalUniqueUri = totalUniqueUri;
        } else {
            System.out.println(ColoredText.YELLOW + "Total unique uri negative. Ignoring Entry!" + ColoredText.RESET);
        }
    }

    public int getTotalUniqueUri() {
        return totalUniqueUri;
    }

    public void setTotalResponseTime(int totalResponseTime) {
        if (totalPost >= 0) {
            this.totalResponseTime = totalResponseTime;
        } else {
            System.out.println(ColoredText.YELLOW + "Total response time negative. Ignoring Entry!" + ColoredText.RESET);
        }
    }

    public int getTotalResponseTime() {
        return totalResponseTime;
    }
}
