package net.therap.model;

import java.util.Comparator;
import java.util.Map.Entry;

/**
 * @author aditya.chakma
 * @since 10/3/21
 */
public class TimeComparator implements Comparator<Entry<Integer, LogSummary>> {

    @Override
    public int compare(Entry<Integer, LogSummary> e1, Entry<Integer, LogSummary> e2) {
        LogSummary r1 = e1.getValue();
        LogSummary r2 = e2.getValue();
        if (r1.getTotalGet() + r1.getTotalPost() > r2.getTotalGet() + r2.getTotalPost()) {
            return -1;
        }

        return 1;
    }
}
