package net.therap.model;

public class TimeConverter {

    public final String TIME_AM = "am";
    public final String TIME_PM = "pm";

    public Time to12(int time) {

        Time time12 = new Time();
        time12.setMeridian(TIME_AM);

        if (time % 12 == 0) {
            time12.setHour(12);
            if (time == 12) {
                time12.setMeridian(TIME_PM);
            }

            return time12;
        }

        time12.setHour(time % 12);

        if (time >= 12) {
            time12.setMeridian(TIME_PM);
        }

        return time12;
    }

    public int to24(int time, String meridian) {

        if (TIME_AM.equals(meridian)) {
            return time % 12;
        }

        return (time + 12) % 12;
    }
}
